__author__ = 'vandermonde'

import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.stats import multivariate_normal

points = np.ndarray(shape=(11, 1), dtype=float, order='c', buffer=np.array([-7, -5, -4, -3, -2, 0, 2, 3, 4.0, 5, 7]))


def computePdfParze(data, x, h, phiFunc='rec'):
    D = data.shape[1]
    Datacount = data.shape[0]
    sampleCount = x.shape[0]

    coved = np.ndarray(shape=(D,D), dtype=float, order='c', buffer=np.array([0 for i in range(D*D)]))
    for i in range(D):
        for j in range(D):
            if i == j:
                coved[i,j] = 1

    k = np.ndarray(shape=(sampleCount, 1), dtype=float, order='c', buffer=np.array([0.0 for i in range(sampleCount)]))

    makhraj = 1/(2*math.pi)**(D/2)

    print("computing : " + str(Datacount) + " * " + str(sampleCount))
    for i in range(Datacount):
        for j in range(sampleCount):
            abs1 = abs(x[j,:] - data[i,:])/h
            if phiFunc == 'gaus':
                absT = abs1.reshape(1, D)
                abs1 = abs1.reshape(D, 1)
                val = makhraj * math.exp(np.matmul(-1 *absT , abs1))
                k[j, 0] += val
            else:
                if np.max(abs1)<0.5:
                    k[j, 0] += 1

    return k/ (h**D * Datacount)


# x = np.ndarray(shape=(141,1), dtype=int, order='c', buffer=np.array([i for i in range(-70, 71, 1)]))
# x = x.astype(float) / 10.0
#
# y1 = computePdfParze(points, x, 1.0/1.0**0.5)
# y2 = computePdfParze(points, x, 2.0/1.0**0.5)
# y3 = computePdfParze(points, x, 4.0/1.0**0.5)
#
# l1, = plt.plot(x, y1, '-', c='red', label='j=1')
# l2, = plt.plot(x, y2, '-', c='blue', label='j=4')
# l3, = plt.plot(x, y3, '-', c='green', label='j=11')
# plt.ylim((0,1))
# plt.legend(handles=[l2, l1, l3])
# plt.show()