__author__ = 'vandermonde'

from hebbLearn import hebbAssociativeLearn
import numpy as np
from hebbLearn import makeNoisy

A = [-1, 1, -1,   1, -1, 1,   1, 1, 1,    1, -1, 1,    1, -1, 1]
B = [1, 1, -1,   1, -1, 1,    1, 1, -1,     1, -1, 1,      1,1,-1]
C = [-1, 1, 1,   1, -1, -1,   1, -1, -1,   1, -1, -1,   -1, 1, 1]
D = [1, 1, -1,    1, -1, 1,    1, -1, 1,    1, -1, 1,    1, 1, -1]
E = [1, 1, 1,    1, -1, -1,    1, 1, -1,    1, -1, -1,    1, 1, 1]
F = [1, 1, 1,    1, -1, -1,   1, 1, -1,    1, -1, -1,    1, -1, -1]
G = [-1, 1, 1,   1, -1, -1,   1, -1, 1,    1, -1, 1,    -1, 1, 1]
H = [1, -1, 1,   1, -1, 1,    1, 1, 1,     1, -1, 1,    1, -1, 1]

aY = [-1, -1, -1]
bY = [-1, -1, 1]
cY = [-1, 1, -1]
dY = [-1, 1, 1]
eY = [1, -1, -1]
fY = [1, -1, 1]
gY = [1, 1, -1]
hY = [1, 1, 1]


trainX = np.empty(shape=(0, 15), dtype=int)
trainX = np.append(trainX, np.array([A]), axis=0)
trainX = np.append(trainX, np.array([B]), axis=0)
trainX = np.append(trainX, np.array([C]), axis=0)
trainX = np.append(trainX, np.array([D]), axis=0)
trainX = np.append(trainX, np.array([E]), axis=0)
trainX = np.append(trainX, np.array([F]), axis=0)
trainX = np.append(trainX, np.array([G]), axis=0)
trainX = np.append(trainX, np.array([H]), axis=0)

trainY = np.empty(shape=(0, 3), dtype=int)
trainY = np.append(trainY, np.array([aY]), axis=0)
trainY = np.append(trainY, np.array([bY]), axis=0)
trainY = np.append(trainY, np.array([cY]), axis=0)
trainY = np.append(trainY, np.array([dY]), axis=0)
trainY = np.append(trainY, np.array([eY]), axis=0)
trainY = np.append(trainY, np.array([fY]), axis=0)
trainY = np.append(trainY, np.array([gY]), axis=0)
trainY = np.append(trainY, np.array([hY]), axis=0)

for i in range(8):
    for j in range(i+1, 8):
        for z in range(j+1, 8):
            for l in range(z+1, 8):
                for m in range(l+1, 8):
                    for n in range(m+1, 8):
                        trainX2 = trainX.copy()
                        trainY2 = trainY.copy()
                        # trainX2 = np.delete(trainX2, (n), axis=0)
                        # trainX2 = np.delete(trainX2, (m), axis=0)
                        # trainX2 = np.delete(trainX2, (l), axis=0)
                        # trainX2 = np.delete(trainX2, (z), axis=0)
                        # trainX2 = np.delete(trainX2, (j), axis=0)
                        # trainX2 = np.delete(trainX2, (i), axis=0)
                        #
                        # trainY2 = np.delete(trainY2, (n), axis=0)
                        # trainY2 = np.delete(trainY2, (m), axis=0)
                        # trainY2 = np.delete(trainY2, (l), axis=0)
                        # trainY2 = np.delete(trainY2, (z), axis=0)
                        # trainY2 = np.delete(trainY2, (j), axis=0)
                        # trainY2 = np.delete(trainY2, (i), axis=0)

                        weights = hebbAssociativeLearn(trainX2, trainY2)

                        print i , j, z, l
                        for k in range(trainX2.shape[0]):
                            testNoisyC = np.ndarray(shape=(1, 15), dtype=int, order='c', buffer=np.array(trainX2[k]))
                            testNoisyC = makeNoisy(testNoisyC, 0.3)
                            print testNoisyC
                            res = np.matmul(testNoisyC, weights)
                            print res
                            print res * trainY2[k]
                        print "###############################"