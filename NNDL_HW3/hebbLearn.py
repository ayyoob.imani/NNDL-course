__author__ = 'vandermonde'


import numpy as np
import random

def hebbAssociativeLearn(trainX, trainY, zeroDiag=False):
    """

    :param trainX: ndarray, (n*DX)
    :param trainY: ndarray, (n*DY)
    :param zeroDiag: wether to set diagonal to 0
    :return: ndarray of weight matrix, (XD*yD)
    """
    vCount = trainX.shape[0]
    xD = trainX.shape[1]
    yD = trainY.shape[1]
    w = np.zeros((xD, yD), np.float)
    for i in range(vCount):
        v1 = trainX[i][np.newaxis].reshape(xD, 1)
        v2 = trainY[i][np.newaxis]
        w += np.matmul(v1, v2)

    if zeroDiag:
        for i in range(xD):
            for j in range(yD):
                if i == j:
                    w[i,j] = 0

    return  w

def makeNoisy(vec, percentage):
    for i in range(vec.shape[0]):
        for j in range(vec.shape[1]):
            if random.random() < percentage:
                vec[i,j] = -vec[i, j]
    return vec