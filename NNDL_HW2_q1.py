__author__ = 'vandermonde'

import numpy as np
import matplotlib.pyplot as plt
from Adaline_learning_algo import *


def activationFunction(number, theta):
    if number > theta:
        return 1
    if number< -theta:
        return -1
    return 0

def perceptronLearn(sArray, tArray, theta, alpha, maxTry=1000):
    """
    :param sArray: training 2D array, each row is one training example each column shows an input neuron
    :param tArray: traget 2D array, each row shows training targets for one training example each column shows an output neuron
    :param theta: theta param for perceptorn algorithem
    :param alpha: alpha learing param for perceptron algorithem
    :return: weights matrix, w[i,j] shows weight for edge between ith input neuron and jth output neuron
    """
    if(len(tArray.shape) ==1):
        outputNeurons = 1
    else:
        outputNeurons = tArray.shape[1]
    inputNeurons = sArray.shape[1]
    trainCount = sArray.shape[0]
    w = np.ndarray(shape=(inputNeurons, outputNeurons), dtype=float, order='c')
    w.fill(0.0)

    for c in range(outputNeurons):
        isFinished = False;
        iterationCount = 0
        while(not isFinished):
            isFinished = True
            for t in range(trainCount):
                if activationFunction(np.sum(sArray[t, : ] * w[:, c]), theta) != tArray[t, c]:
                    w[:, c] += sArray[t, :] * tArray[t, c] * alpha
                    isFinished = False
                # print(w[:, c])
            if(iterationCount > maxTry):
                print("couldn't find proper weight for output neuron number: " + str(c))
                break;
            iterationCount += 1

        print("learning for output neuron number: ", c, "done in ", iterationCount, "steps")
    return w


# UChar = np.array([1,-1,1,  1,-1,1,   1,1,1])
# IChar = np.array([-1,1,-1,   -1,1,-1,   -1,1,-1])
# OChar = np.array([1,1,1,   1,-1,1,   1,1,1])
# LChar = np.array([-1,1,-1,   -1,1,-1,   -1,1,1])
#
# tData = np.array([UChar, IChar, OChar, LChar])
# uTarget = np.ndarray(shape=(4,1), dtype=int, order='c', buffer=np.array([1, -1, -1, -1]))
#
# w = perceptronLearn(tData, uTarget, 0.2, 1)
#
# print(w)
#
# print("U: ", activationFunction(np.sum(w[:, 0] * UChar), 0.2))
# print("I: ", activationFunction(np.sum(w[:, 0] * IChar), 0.2))
# print("O: ", activationFunction(np.sum(w[:, 0] * OChar), 0.2))
# print("L: ", activationFunction(np.sum(w[:, 0] * LChar), 0.2))

####################################################################################
###########################     QUESTION 2      ####################################
###########################                     ####################################

# apple = np.array([-1, 1, 1])
# orange = np.array([1, 1, 1])
# pear = np.array([-1, -1, 1])
# trainData = np.array([apple, orange, pear])
# targetValues = np.ndarray(shape=(3,3), order='c',dtype=int, buffer=np.array([1, -1, -1,   -1, 1, -1,   -1, -1, 1]))
#
# w = perceptronLearn(trainData, targetValues, 0.2, 1)
# print(w)
#
# print("Apple is apple? ", activationFunction(np.sum(w[:, 0] * apple), 0.2))
# print("Apple is orange? ", activationFunction(np.sum(w[:, 1] * apple), 0.2))
# print("Apple is pear? ", activationFunction(np.sum(w[:, 2] * apple), 0.2))
#
# print("Orange is apple? ", activationFunction(np.sum(w[:, 0] * orange), 0.2))
# print("Orange is orange? ", activationFunction(np.sum(w[:, 1] * orange), 0.2))
# print("Orange is pear? ", activationFunction(np.sum(w[:, 2] * orange), 0.2))
#
# print("pear is apple? ", activationFunction(np.sum(w[:, 0] * pear), 0.2))
# print("pear is orange? ", activationFunction(np.sum(w[:, 1] * pear), 0.2))
# print("pear is pear? ", activationFunction(np.sum(w[:, 2] * pear), 0.2))



###########################################################################################
###########################     QUESTION 3 PART 1      ####################################
###########################                            ####################################
# blue_med = np.random.randn(100, 2)
# blue_med = 0.5 * blue_med + 1
#
# red_med = np.random.randn(100, 2)
# red_med = 0.5 * red_med - 1
#
# train_data = np.zeros((200, 3))
# train_data.fill(1)
# train_data[:100, :2] = blue_med
# train_data[100:, :2] = red_med
#
# target_data = np.zeros((200, 1))
# target_data[:100, :] = 1
# target_data[100:, :] = -1
#
# plt.plot(blue_med[:, 0], blue_med[:, 1], 'bo')
# plt.plot(red_med[:, 0], red_med[:, 1], 'ro')
# plt.xlabel('x1')
# plt.ylabel('x2')
#
# weights = perceptronLearn(train_data, target_data, 0, 1)
# print("perceptron weights", weights)
# lineDotX1 = [-2, 2]
# lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
# l3, = plt.plot(lineDotX1, lineDotX2, 'y-', label='perceptron')
#
# weights = adalineLearn(train_data, target_data, 0.7/200, 0.001)
# print("adaline sign weights", weights)
# lineDotX1 = [-2, 2]
# lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
# l2, = plt.plot(lineDotX1, lineDotX2, 'g-', label='delta_sign')
#
# weights = adalineSigmoidLearn(train_data, target_data, 0.7/200, 0.001, 1)
# print("adaline sigmoid weights", weights)
# lineDotX1 = [-2, 2]
# lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
# l1, = plt.plot(lineDotX1, lineDotX2, 'c-', label='delta_sigmoid')
#
# plt.legend(handles=[l2, l1, l3])
# plt.show()
#







######################################################################################
###############################    Question 3 part 2     #############################
######################################################################################
blue_big = np.random.randn(1000, 2)
blue_big = 0.5 * blue_big + 1

red_s = np.random.randn(10, 2)
red_s = 0.5 * red_s - 1

train_data = np.zeros((1010, 3))
train_data.fill(1)
train_data[:1000, :2] = blue_big
train_data[1000:, :2] = red_s

target_data = np.zeros((1010, 1))
target_data[:1000, :] = 1
target_data[1000:, :] = -1

plt.plot(blue_big[:, 0], blue_big[:, 1], 'bo')
plt.plot(red_s[:, 0], red_s[:, 1], 'ro')
plt.xlabel('x1')
plt.ylabel('x2')


weights = perceptronLearn(train_data, target_data, 0, 1)
print("perceptron weights", weights)
lineDotX1 = [-2, 2]
lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
l3, = plt.plot(lineDotX1, lineDotX2, 'y-', label='perceptron')

weights = adalineLearn(train_data, target_data, 0.7/200, 0.001)
print("adaline sign weights", weights)
lineDotX1 = [-2, 2]
lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
l2, = plt.plot(lineDotX1, lineDotX2, 'g-', label='delta_sign')

weights = adalineSigmoidLearn(train_data, target_data, 0.7/200, 0.001, 1)
print("adaline sigmoid weights", weights)
lineDotX1 = [-2, 2]
lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]
l1, = plt.plot(lineDotX1, lineDotX2, 'c-', label='delta_sigmoid')

plt.legend(handles=[l2, l1, l3])
plt.show()




######################################################################################
###############################    Question 4     #############################
######################################################################################

# innerDots = np.ndarray(shape=(8,3), dtype=float, order='c', buffer=np.array([5.9, 1.2, 1,    3.1, 1, 1,    1,1,1,     1,-0.9,1,    0.2, -2, 1,      -0.2, 0.2, 1,    -2,-3,1,   -3,0.9,1]))
#
# buf = np.array([9.5,1,1,   8,0.2,1,   7,-1,1,   4,-2.4, 1,    1.5, -3,1,    -1,-4,1,   -3,-3,1,  -5,-1,1,  -3.5,-1.2,1,    -2.5, 3,1,   -1,2.5,1,    0.9,3,1,   2.5,3.5,1,   3.5,2.5,1,   3.5,2.8,1,   5,3,1,   6.5,2.8,1,   8,3,1 ])
# outerDots = np.ndarray(shape =(16,3), dtype=float, order='c', buffer=buf)
#
# plt.plot(innerDots[:, 0], innerDots[:, 1], 'bo')
# plt.plot(outerDots[:, 0], outerDots[:, 1], 'ro')
# plt.xlabel('x1')
# plt.ylabel('x2')




# squares = np.array([[1,2,1,1],  [1,2,-1,1],  [1,-1,1,1],  [1,-1,-1,1]])
# circle = np.array([ [0,0,0,1] ])
# circle = circle.reshape(1,4)
# train=np.concatenate((squares, circle), axis=0)
# target = np.array([1,1,1,1,-1])
# target = target.reshape(5,1)
#
# weights = perceptronLearn(train, target, 0.2, 1)
# print(weights)


# plt.show()