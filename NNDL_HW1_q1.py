__author__ = 'vandermonde'

import numpy as np
import matplotlib.pyplot as plt

SArray = np.ndarray(shape=(4,3), dtype=int, order='c', buffer=np.array([1, 1, 1, 1, -1, 1, -1, 1, 1, -1, -1, 1]))
TArray = np.array([1,-1,-1,-1])

def hebbLearn(sArray, tArray, nClasses=2):
    w = np.ndarray(shape=(sArray.shape[1], nClasses-1), dtype=float, order='c')
    w.fill(0.0)
    for i in range(sArray.shape[0]):
        for j in range(nClasses-1):
            w[:,j] += sArray[i, :] * tArray[i]
    return w



weights = hebbLearn(SArray, TArray)
lineDotX1 = [-10, 10]
lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]

greenDots = SArray[TArray==-1,0:2]
redDots = SArray[TArray==1,0:2]


plt.plot(lineDotX1, lineDotX2, 'b-')
plt.plot(greenDots[:, 0], greenDots[:, 1], 'go')
plt.plot(redDots[:, 0], redDots[:, 1], 'ro')
plt.xlabel('X1')
plt.ylabel('x2')
plt.show()



