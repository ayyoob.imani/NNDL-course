__author__ = 'vandermonde'

from sklearn.multiclass import OneVsOneClassifier
from sklearn.linear_model import Perceptron
from TinyMNIST_loader import *
from sklearn.metrics import accuracy_score
from plot_conf_mat import plot_confusion_matrix
import matplotlib.pyplot as plt

def getConfustionConfidenceMatrix(trueLabels, testLabels, classes, confidencesvals=None):
    cCount = len(classes)
    confusion = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))
    if confidencesvals!=None:
        confidences = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))

    for i in range(len(testLabels)):
        confusion[trueLabels[i], testLabels[i]] += 1
        if confidencesvals!=None:
            confidences[trueLabels[i], testLabels[i]] += confidencesvals[i]

    normalizedConfusion = confusion.copy()
    if confidencesvals!=None:
        normalizedConfidence = confidences.copy()

    for i in range(cCount):
        np_sum = np.sum(confusion[i, :])
        if np_sum == 0:
            np_sum = 1
        normalizedConfusion[i, :] /= np_sum
        for j in range(cCount):
            if confusion[i,j] == 0:
                pass
            else:
                pass
                if confidencesvals!=None:
                    normalizedConfidence[i,j] /= confusion[i,j]

    plt.figure()
    plot_confusion_matrix(confusion, classes=classes,
                          title='Confusion matrix, without normalization')
    plt.show()
    plot_confusion_matrix(normalizedConfusion, classes=classes,
                          title='Normalized Confusion', normalize=True)
    plt.show()
    if confidencesvals!=None:
        plot_confusion_matrix(normalizedConfidence, classes=classes,
                              title='Normalized Confidence', normalize=True)
        plt.show()


class_names = [i for i in range(10)]
clf = OneVsOneClassifier(Perceptron())
clf.fit(train_data, train_labels)

res = clf.predict(test_data)

print accuracy_score(test_labels, res)
getConfustionConfidenceMatrix(test_labels, res, class_names)