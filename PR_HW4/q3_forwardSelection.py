__author__ = 'vandermonde'

from TinyMNIST_loader import *
from BayesClassifier import *
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

class_names = [i for i in range(10)]

dims = [i for i in range(train_data.shape[1])]
selectedDims = np.array([], dtype=int)
bestImprove = 1
maxAccuracy = 0
selectedDim = -1
ccr = []
counter = 0
while True:
    counter += 1
    if counter == 60:
        break

    print bestImprove
    if selectedDim != -1:
        selectedDims = np.append(selectedDims, [selectedDim])
        if selectedDim in dims:
            dims.remove(selectedDim)
    print selectedDims

    bestImprove = -1
    for dim in dims:
        currentDims = np.append(selectedDims, dim)
        data = train_data[:, currentDims]

        res, confidences = bayesClassifier(data, train_labels, test_data[:, currentDims], class_names, pdfMethod='ML')
        accuracy = accuracy_score(test_labels, res)

        improve =  accuracy - maxAccuracy
        if improve > bestImprove:
            bestImprove = improve
            selectedDim = dim

    ccr.append(bestImprove)

print "###########"
print ccr
print selectedDims
plt.plot(ccr)
plt.show()