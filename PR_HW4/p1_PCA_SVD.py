__author__ = 'vandermonde'

from Load_Data import *
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg

def DescendingSortEigns(eign_values, eign_vectors):
    idx = eign_values.argsort()[::-1]
    eign_values = eign_values[idx]
    eign_vectors = eign_vectors[:,idx]
    return eign_values , eign_vectors


data = getImages("face")
print data.shape
means = np.mean(data, axis=0)
normalizedData = data - means

####### singular value decomposition
U,D,V = linalg.svd(normalizedData,full_matrices=False)
diagD = np.diag(D)

###### 10 first eigen faces
for i in range(10):
    m=V[i, :]
    sss = m.reshape(112,92)
    plt.subplot(2,5,i+1)
    plt.imshow(sss,cmap='gray')
    plt.title("ten first eigenfaces")
plt.show()

####### 10 last eigen faces
for i in range(390, 400):
    m=V[i, :]
    sss = m.reshape(112,92)
    plt.subplot(2,5,i-389)
    plt.imshow(sss,cmap='gray')
    plt.title("ten last eigenfaces")
plt.show()


####### eigen values
plt.plot(D)
plt.show()

######### PCA TRANSFORM ######
reducedV = V[:50, :]
transformedData = np.matmul(normalizedData, np.transpose(reducedV))

######## inverse transform ##########
reconstructedData = np.matmul(transformedData, reducedV)
reconstructedData += means

reducedPic = reconstructedData[0,:].reshape(112,92)
plt.subplot(1,2,1)
plt.imshow(reducedPic,cmap='gray')
plt.title("reduced picture")

origPic = data[0,:].reshape(112,92)
plt.subplot(1,2,2)
plt.imshow(origPic,cmap='gray')
plt.title("original picture")
plt.show()

reducedPic = reconstructedData[200,:].reshape(112,92)
plt.subplot(1,2,1)
plt.imshow(reducedPic,cmap='gray')
plt.title("reduced picture")

origPic = data[200,:].reshape(112,92)
plt.subplot(1,2,2)
plt.imshow(origPic,cmap='gray')
plt.title("original picture")
plt.show()