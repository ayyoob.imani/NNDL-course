__author__ = 'vandermonde'

import os
import matplotlib.image as mpimg
import numpy as np

def getImages(dir):
    vectors = []
    subdirs = os.listdir(dir)
    for i in subdirs:
        images = os.listdir(dir + "/" + i)
        for img in images:
            image = mpimg.imread(dir + "/" + i + '/'+img)
            image = image[:, :, 0]
            image = np.array(image).flatten()
            vectors.append(image)
    res = np.matrix((vectors))
    return res

