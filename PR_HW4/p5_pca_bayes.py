__author__ = 'vandermonde'

from TinyMNIST_loader import *
from BayesClassifier import *
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from numpy import linalg
import numpy as np

class_names = [i for i in range(10)]


def DescendingSortEigns(eign_values, eign_vectors):
    idx = eign_values.argsort()[::-1]
    eign_values = eign_values[idx]
    eign_vectors = eign_vectors[:,idx]
    return eign_values , eign_vectors



print train_data.shape
means = np.mean(train_data, axis=0)
normalizedData = train_data - means
testMeans = np.mean(test_data, axis=0)
normalizedTestData = test_data - testMeans

####### singular value decomposition
U,D,V = linalg.svd(normalizedData,full_matrices=False)

print "U", U.shape
print "D", D.shape
print "V", V.shape
diagD = np.diag(D)

plt.plot(D)
plt.show()

######### PCA TRANSFORM ######
reducedV = V[:20, :]
transformedData = np.matmul(normalizedData, np.transpose(reducedV))
transformedTestData = np.matmul(normalizedTestData, np.transpose(reducedV))

res, confidences = bayesClassifier(transformedData, train_labels, transformedTestData, class_names, pdfMethod='ML')
print "with PCA accuracy: " , accuracy_score(test_labels, res)

res, confidences = bayesClassifier(train_data, train_labels, test_data, class_names, pdfMethod='ML')
print "without PCA accuracy: " , accuracy_score(test_labels, res)
