__author__ = 'vandermonde'

from TinyMNIST_loader import *
from numpy import linalg as LA
from math import sqrt
import matplotlib.pyplot as plt


def sortEigens(vals, vecs):
    idx = vals.argsort()[::-1]
    vals = vals[idx]
    vecs = vecs[:,idx]
    return vals , vecs


def diagMatSquareRoot(D):
    for i in range(D.shape[0]):
        if D[i,i] != 0:
            D[i,i] = 1.0/sqrt(D[i,i])

def withinScatterMatrix(data, labels, labelNames):

    res = np.zeros((data.shape[1], data.shape[1]))
    for cls in labelNames:
        clsData = data[np.where(labels == cls)[0],:]
        coved = np.cov(clsData, rowvar=False)
        res += coved * (clsData.shape[0]-1)

    return res

def betweenScatterMatrix(data, labels, labelNames):
    dataMean = np.mean(data, axis=0)
    res = np.zeros((data.shape[1], data.shape[1]), dtype=float)
    for cls in class_names:
        clsData = data[np.where(labels == cls)[0],:]
        clsMean = np.mean(clsData, axis=0)
        res += np.matmul((clsMean-dataMean).reshape(data.shape[1],1), (clsMean-dataMean).reshape(1,data.shape[1])) * clsData.shape[0]
    return res

covd = np.cov(train_data, rowvar=False)
D, V = LA.eig(covd)
D, V = sortEigens(D, V)
D = np.diag(D)

diagMatSquareRoot(D)

projectionMat = np.matmul(D, np.transpose(V))

train_mean = np.mean(train_data, axis=0).reshape(1,train_data.shape[1])
test_mean = np.mean(test_data, axis=0).reshape(1,test_data.shape[1])

zero_mean_train_Data = np.transpose((train_data - train_mean))
projected_train_data = np.transpose(np.matmul(projectionMat, zero_mean_train_Data))

zero_mean_test_Data = np.transpose((test_data - test_mean))
projected_testData = np.transpose(np.matmul(projectionMat, zero_mean_test_Data))

#############################################
class_names = [i for i in range(10)]
Sw = withinScatterMatrix(projected_train_data, train_labels, class_names)
Sb = betweenScatterMatrix(projected_train_data, train_labels, class_names)
Sp = np.matmul(np.linalg.inv(Sw), Sb)
print Sp

Sd, Sv = LA.eig(Sp)
Sd, Sv = sortEigens(Sd, Sv)
plt.plot(Sd)
plt.show()
sD = np.diag(Sd)

trace = np.sum(sD)
separability = [0 for i in range(sD.shape[0])]
for i in range(len(separability)):
    separability[i] += separability[i-1] + sD[i,i]/trace

plt.plot(separability)
plt.show()
