from TinyMNIST_loader import *
from Adaline_learning_algo import *
from sklearn.metrics import accuracy_score
import operator
import random
from plot_conf_mat import plot_confusion_matrix


def activationFunction(number, theta):
    if number > theta:
        return 1
    if number< -theta:
        return -1
    return 0

def getConfustionConfidenceMatrix(trueLabels, testLabels, classes, confidencesvals=None):
    cCount = len(classes)
    confusion = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))
    if confidencesvals!=None:
        confidences = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))

    for i in range(len(testLabels)):
        confusion[trueLabels[i], testLabels[i]] += 1
        if confidencesvals!=None:
            confidences[trueLabels[i], testLabels[i]] += confidencesvals[i]

    normalizedConfusion = confusion.copy()
    if confidencesvals!=None:
        normalizedConfidence = confidences.copy()

    for i in range(cCount):
        np_sum = np.sum(confusion[i, :])
        if np_sum == 0:
            np_sum = 1
        normalizedConfusion[i, :] /= np_sum
        for j in range(cCount):
            if confusion[i,j] == 0:
                pass
            else:
                pass
                if confidencesvals!=None:
                    normalizedConfidence[i,j] /= confusion[i,j]

    plt.figure()
    plot_confusion_matrix(confusion, classes=classes,
                          title='Confusion matrix, without normalization')
    plt.show()
    plot_confusion_matrix(normalizedConfusion, classes=classes,
                          title='Normalized Confusion', normalize=True)
    plt.show()
    if confidencesvals!=None:
        plot_confusion_matrix(normalizedConfidence, classes=classes,
                              title='Normalized Confidence', normalize=True)
        plt.show()



def perceptronLearn(sArray, tArray, theta, alpha, maxTry=1000):
    """
    :param sArray: training 2D array, each row is one training example each column shows an input neuron
    :param tArray: target 2D array, each row shows training targets for one training example each column shows an output neuron
    :param theta: theta param for perceptorn algorithem
    :param alpha: alpha learing param for perceptron algorithem
    :return: weights matrix, w[i,j] shows weight for edge between ith input neuron and jth output neuron
    """
    if(len(tArray.shape) ==1):
        outputNeurons = 1
    else:
        outputNeurons = tArray.shape[1]
    inputNeurons = sArray.shape[1]
    trainCount = sArray.shape[0]
    w = np.ndarray(shape=(inputNeurons, outputNeurons), dtype=float, order='c')
    w.fill(0.0)

    for c in range(outputNeurons):
        isFinished = False;
        iterationCount = 0
        while(not isFinished):
            isFinished = True
            for t in range(trainCount):
                if activationFunction(np.sum(sArray[t, : ] * w[:, c]), theta) != tArray[t, c]:
                    w[:, c] += sArray[t, :] * tArray[t, c] * alpha
                    isFinished = False
                # print(w[:, c])
            if(iterationCount > maxTry):
                print("couldn't find proper weight for output neuron number: " + str(c))
                break;
            iterationCount += 1

        print("learning for output neuron number: ", c, "done in ", iterationCount, "steps")
    return w


print "################ one vs all C###############"
class_names = [i for i in range(10)]
trainData1 = train_data.copy()
x = np.ones((trainData1.shape[0], 1), dtype=float)
trainData1 = np.concatenate((trainData1, x), axis=1)
testData1 = test_data.copy()
x = np.ones((testData1.shape[0], 1), dtype=float)
testData1 = np.concatenate((testData1, x), axis=1)
trainLabels1 = train_labels.copy()
res = np.array([-1 for i in range(len(test_labels))])
for cls in class_names:
    labels = np.ndarray(shape=(trainData1.shape[0], 1), buffer=np.array([-1.0 for i in range(trainData1.shape[0])]), dtype=float, order='c')
    labels[np.where(trainLabels1 == cls), 0] = 1

    trainData2 = trainData1.copy()
    labels2 = labels.copy()
    indexes = [i for i in range(trainData2.shape[0])]
    random.shuffle(indexes)
    trainData2 = trainData2[indexes, :]
    labels2 = labels2[indexes, :]

    w = perceptronLearn(trainData2, labels2, 0, 0.005, maxTry=500)

    for i in range(len(test_labels)):
        if res[i] == -1:
            if activationFunction(np.sum(w[:,0]*testData1[i,:]), 0) == 1:
                res[i] = cls

    toRemain = np.where(trainLabels1 != cls)[0]
    trainData1 = trainData1[toRemain,:]
    trainLabels1 = trainLabels1[toRemain]



print "one vs all C class: ", accuracy_score(test_labels, res)
getConfustionConfidenceMatrix(test_labels, res, class_names)

res[np.where(res == -1)] = 9
print "one vs all C-1 class: ", accuracy_score(test_labels, res)
getConfustionConfidenceMatrix(test_labels, res, class_names)

print "############### one vs one ###############"
trainData1 = train_data.copy()
x = np.ones((trainData1.shape[0], 1), dtype=float)
trainData1 = np.concatenate((trainData1, x), axis=1)
testData1 = test_data.copy()
x = np.ones((testData1.shape[0], 1), dtype=float)
testData1 = np.concatenate((testData1, x), axis=1)
trainLabels1 = train_labels.copy()
res = np.array([-1 for i in range(len(test_labels))])
votes = [{0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0} for i in range(testData1.shape[0])]
print votes
for idx1 in range(len(class_names)):
    cls1 = class_names[idx1]
    for idx2 in range(idx1+1, len(class_names)):
        cls2 = class_names[idx2]
        if cls2 != cls1:
            c1Idx = np.where(train_labels == cls1)[0]
            c2Idx = np.where(train_labels == cls2)[0]
            c1Data = trainData1[c1Idx, :]
            c2Data = trainData1[c2Idx, :]
            labels = np.array([-1 for i in range(c1Data.shape[0] + c2Data.shape[0])])
            labels[:c1Data.shape[0]] = 1
            trains = np.concatenate((c1Data, c2Data), axis=0)
            indexes = [i for i in range(trains.shape[0])]
            random.shuffle(indexes)
            trains = trains[indexes, :]
            labels = labels.reshape(len(indexes), 1)[indexes, :]
            w = perceptronLearn(trains, labels.reshape(c1Data.shape[0] + c2Data.shape[0], 1), 0, 0.005, maxTry=1000)

            for i in range(len(test_labels)):
                if activationFunction(np.sum(w[:,0]*testData1[i,:]), 0) == 1:
                    votes[i][cls1] += 1
                else:
                    votes[i][cls2] += 1

for i in range(len(test_labels)):
    bestCls = max(votes[i].iteritems(), key=operator.itemgetter(1))[0]
    res[i] = bestCls

print "one vs one: ", accuracy_score(test_labels, res)

getConfustionConfidenceMatrix(test_labels, res, class_names)