__author__ = 'vandermonde'

import numpy as np
import matplotlib.pyplot as plt


def bipolarFunction(number, theta):
    if number > theta:
        return 1
    if number< -theta:
        return -1
    return 0

def bipolarSigmoid(number, gamma):
    return np.tanh(gamma * number)

def isSignificant(deltaW, threshold):
    for i in deltaW:
        if abs(i) > threshold:
            return True
    return False


def adalineLearn(sArray, tArray, learnRate, threshold, maxTry=1000):
    """
    :param sArray: training 2D array, each row is one training example each column shows an input neuron
    :param tArray: traget 2D array, each row shows training targets for one training example each column shows an output neuron
    :param theta: theta param for perceptorn algorithem
    :param learnRate: alpha learing param for perceptron algorithem
    :return: weights matrix, w[i,j] shows weight for edge between ith input neuron and jth output neuron
    """

    outputNeurons = tArray.shape[1]
    inputNeurons = sArray.shape[1]
    trainCount = sArray.shape[0]
    w = np.random.rand(inputNeurons, outputNeurons)
    w.fill(0.0)

    for c in range(outputNeurons):
        isFinished = False;
        iterationCount = 0
        while(not isFinished):
            isFinished = True
            prevW = w.copy()
            for t in range(trainCount):
                deltaW = (tArray[t, c] - np.sum(sArray[t, : ] * w[:, c])) * sArray[t, :]  * learnRate
                w[:, c] += deltaW

            if isSignificant(w - prevW, threshold):
                isFinished = False

            if(iterationCount > maxTry):
                print("couldn't find proper weight for output neuron number: " + str(c))
                break

            iterationCount += 1

        print("learning for output neuron number: ", c, "done in ", iterationCount, "steps")
    return w


def adalineSigmoidLearn(sArray, tArray, learnRate, threshold, gamma, maxTry=10000):
    """
    :param sArray: training 2D array, each row is one training example each column shows an input neuron
    :param tArray: traget 2D array, each row shows training targets for one training example each column shows an output neuron
    :param theta: theta param for perceptorn algorithem
    :param learnRate: alpha learing param for perceptron algorithem
    :return: weights matrix, w[i,j] shows weight for edge between ith input neuron and jth output neuron
    """

    outputNeurons = tArray.shape[1]
    inputNeurons = sArray.shape[1]
    trainCount = sArray.shape[0]
    w = np.random.rand(inputNeurons, outputNeurons)
    w.fill(0.0)

    for c in range(outputNeurons):
        isFinished = False;
        iterationCount = 0
        while(not isFinished):
            isFinished = True
            prevW = w.copy()
            for t in range(trainCount):
                net_sum = np.sum(sArray[t, :] * w[:, c])
                tanh = np.tanh(gamma* net_sum)
                deltaW = (tArray[t, c] - tanh) * (1 - tanh*tanh) * sArray[t, :]  * learnRate * gamma
                w[:, c] += deltaW

            if isSignificant(w - prevW, threshold):
                isFinished = False

            if(iterationCount > maxTry):
                print("couldn't find proper weight for output neuron number: " + str(c))
                break

            iterationCount += 1
            if iterationCount % 10:
                gamma += 1

        print("learning for output neuron number: ", c, "done in ", iterationCount, "steps")
    return w


def customMadalineLearn(sArray, tArray, learnRate, threshold, maxTry=10000):
    """
    :param sArray: training 2D array, each row is one training example each column shows an input neuron
    :param tArray: traget 2D array, each row shows training targets for one training example each column shows an output neuron
    :param theta: theta param for perceptorn algorithem
    :param learnRate: alpha learing param for perceptron algorithem
    :return: weights matrix, w[i,j] shows weight for edge between ith input neuron and jth output neuron
    """

    outputNeurons = tArray.shape[1]
    inputNeurons = sArray.shape[1]
    trainCount = sArray.shape[0]
    w = np.random.rand(inputNeurons, outputNeurons)
    w.fill(0.0)

    for c in range(outputNeurons):
        isFinished = False;
        iterationCount = 0
        while(not isFinished):
            isFinished = True
            prevW = w.copy()
            for t in range(trainCount):
                z1 = np.sum(sArray[t, : ] * w[:, 0])
                z2 = np.sum(sArray[t, : ] * w[:, 1])
                z3 = np.sum(sArray[t, : ] * w[:, 2])
                y = z1 - z2 -z3 -1/2
                deltaW = (tArray[t, c] - np.sum(sArray[t, : ] * w[:, c])) * sArray[t, :]  * learnRate
                w[:, c] += deltaW

            if isSignificant(w - prevW, threshold):
                isFinished = False

            if(iterationCount > maxTry):
                print("couldn't find proper weight for output neuron number: " + str(c))
                break

            iterationCount += 1

        print("learning for output neuron number: ", c, "done in ", iterationCount, "steps")
    return w


