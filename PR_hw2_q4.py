__author__ = 'vandermonde'
import numpy as np
import math



def f(x,mu,sigma2):
    return (1/math.sqrt(2*math.pi*sigma2)) * pow(math.e , (pow(x-mu , 2)*-1) / (2*sigma2))


def calculateMU(xVals , gammaVector , gammaTots):
    mu = []
    for k in range(len(gammaTots)):
        mu.append(0)
    for k in range(len(gammaTots)):
        for n in range(len(xVals)):
            mu[k] += gammaVector[k][n]*xVals[n]
        mu[k] = (mu[k]/gammaTots[k])
    return mu

def calculateGamma(xVals, sdVals, meanVals, PIVals):
    gammaVector = []

    for k in range(len(PIVals)):
        gammaVector.append([])
        for n in range(len(xVals)):
            temp = p_estimate_helper(xVals[n] , sdVals[k] , meanVals[k] ,PIVals[k] , sdVals, meanVals, PIVals)
            gammaVector[k].append(temp)

    return gammaVector


def p_estimate_helper(x, variance, mean, p, all_var, all_mean, all_p):
    soorat = f(x , mean , variance)*p
    makhraj = 0
    for j in range(len(all_mean)):
        makhraj += f(x ,all_mean[j] , all_var[j])*all_p[j]
    makhraj = 0.000000001 if makhraj==0 else makhraj

    return soorat/makhraj


def calCulatePI(xVals, PIVals , gammaTots):
    res =[]
    for k in range(len(PIVals)):
        res.append(gammaTots[k]/len(xVals))
    return res


def calculateSigma(all_x, gammaVector , mean_estimation , gamma):
    sigma = []
    for i in range(len(gamma)):
        sigma.append(0)

    for k in range(len(gamma)):
        for n in range(len(all_x)):
            sigma[k] += gammaVector[k][n]*pow((all_x[n] - mean_estimation[k]),2)

        sigma[k] = sigma[k]/(gamma[k])

    return sigma



def getSamples(sampleCount):
    res = []
    for i in range(0,sampleCount,4):
        tmp = np.random.normal(1.0 , math.sqrt(0.01))
        res.append(tmp)
        tmp = np.random.normal(1.4 , math.sqrt(0.13))
        res.append(tmp)
        tmp = np.random.normal(2.0 , math.sqrt(0.05))
        res.append(tmp)
        tmp = np.random.normal(3.3 , math.sqrt(0.02))
        res.append(tmp)

    return res

samples = getSamples(400)


PIVals = [0.25,0.25, 0.25, 0.25 ]
meanVals = [1,1.5, 2, 4]
all_variance = [0.1,0.3, 0.12, 0.01]

iterCount = 5000
for i in range(iterCount):
    print("*****************      iter: " + str(i+1) + "     *****************")
    gammaVector = calculateGamma(samples, all_variance, meanVals, PIVals)

    gammaTots = [0 for i in range(len(PIVals))]
    for i in range(len(PIVals)):
        for j in range(len(samples)):
            gammaTots[i] += gammaVector[i][j]


    PIVals = calCulatePI(samples ,PIVals , gammaTots)
    meanTemp = calculateMU(samples , gammaVector, gammaTots)
    all_variance = calculateSigma(samples ,gammaVector, meanTemp , gammaTots)
    meanVals = meanTemp

    print("meanVals: ", meanVals)
    print("variances: ", all_variance)


