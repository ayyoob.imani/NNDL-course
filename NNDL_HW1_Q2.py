__author__ = 'vandermonde'

import numpy as np
import matplotlib.pyplot as plt

SArray = np.ndarray(shape=(12,4), dtype=float, order='c', buffer=np.array([0.05, 0.5, 1, -1, 0.1, 0.7, 1, 1, 0.12, 1, 1, -1, 0.2, 0.5, 1, 1, 0.22, 0.31, 1, 1, 0.35, 0.38, 1, 1, 0.4, 0.75, 1, -1, 0.45, 0.5, 1, 1, 0.55, 0.22, 1, -1, 0.7, 0.66, 1, -1, 0.8, 0.26, 1, -1, 0.92, 0.42, 1, -1]))
TArray = SArray[:, 3]
SArray = SArray[:, 0:3]


def hebbLearn(sArray, tArray, nClasses=2):
    w = np.ndarray(shape=(sArray.shape[1], nClasses-1), dtype=float, order='c')
    w.fill(0.0)
    for i in range(sArray.shape[0]):
        for j in range(nClasses-1):
            w[:,j] += sArray[i, :] * tArray[i]
    return w



weights = hebbLearn(SArray, TArray)
print(weights)
lineDotX1 = [-1, 1]
lineDotX2 = [float(-weights[0,0]*lineDotX1[0]-weights[2,0])/weights[1,0], float(-weights[0,0]*lineDotX1[1]-weights[2,0])/weights[1,0]]

greenDots = SArray[TArray==-1,0:2]
redDots = SArray[TArray==1,0:2]


plt.plot(lineDotX1, lineDotX2, 'b-')
plt.plot(greenDots[:, 0], greenDots[:, 1], 'go')
plt.plot(redDots[:, 0], redDots[:, 1], 'ro')
plt.xlabel('i1')
plt.ylabel('i2')
plt.show()





