__author__ = 'vandermonde'

import numpy as np
from sklearn.feature_selection import VarianceThreshold
import math
from scipy.stats import multivariate_normal
from PR_HW3_P2_parzen import computePdfParze
from PR_HW3_P5_knnPDF import findKnn
from PR_HW3_P5_knnPDF import getConfustionConfidenceMatrix

# Loading Dataset
train_data = np.loadtxt('TinyMNIST/newTrain.txt', dtype=np.float32)
train_labels = np.loadtxt('TinyMNIST/newTrainLabels.txt', dtype=np.int32)
test_data = np.loadtxt('TinyMNIST/newTest.txt', dtype=np.float32)
test_labels = np.loadtxt('TinyMNIST/newTestLabels.txt', dtype=np.int32)
class_names = [i for i in range(10)]
#
# tr_samples_size, feature_size = train_data.shape
# te_samples_size, _ = test_data.shape
# print('Train Data Samples:',tr_samples_size,
#       ', Test Data Samples',te_samples_size,
#       ', Feature Size(after feature-selection):', feature_size)

# np.savetxt("TinyMNIST/newTrain.txt", train_data)
# np.savetxt("TinyMNIST/newTrainLabels.txt", train_labels)
# np.savetxt("TinyMNIST/newTest.txt", test_data)
# np.savetxt("TinyMNIST/newTestLabels.txt", test_labels)

# train_data = np.loadtxt('TinyMNIST/trainData.csv', dtype=np.float32, delimiter=',')
# train_labels = np.loadtxt('TinyMNIST/trainLabels.csv', dtype=np.int32, delimiter=',')
# test_data = np.loadtxt('TinyMNIST/testData.csv', dtype=np.float32, delimiter=',')
# test_labels = np.loadtxt('TinyMNIST/testLabels.csv', dtype=np.int32, delimiter=',')



def findPriors(labels, classes):
    priors = {}
    for i in classes:
        priors[i] = float(len(np.where(labels == i)[0]))/len(labels)
    return priors

def findGaussianPostriorParams(data, labels, classes):
    postriorParams = {}
    pooledMatrix = None
    count = 0
    landa = 0.05
    for i in classes:
        objs = data[np.where(labels == i)[0], :]
        covd = np.cov(objs, rowvar=False)
        postriorParams[i] = [np.mean(objs, axis=0), covd]
        if count == 0:
            pooledMatrix = (objs.shape[0] -1) * covd
        else:
            pooledMatrix += (objs.shape[0] -1) * covd
        count += 1

    for i in classes:
        objs = data[np.where(labels == i)[0], :]
        postriorParams[i][1] = ( (1-landa) * (objs.shape[0]-1) * postriorParams[i][1] + landa *pooledMatrix )/ ( (1-landa) * (objs.shape[0]) + landa*data.shape[0])

    return postriorParams

def getPosteriors(train, trainLabels, test, classes, pdfMethod, parzenWindow, phiFunction, cls, k):

    testSize = test.shape[0]

    if pdfMethod=='parzen':
            return computePdfParze(train[np.where(trainLabels==cls)[0],:], test, parzenWindow, phiFunction)
    if pdfMethod == 'ML':
        res = []
        postriorParams = findGaussianPostriorParams(train, trainLabels, classes)
        var = multivariate_normal(mean=postriorParams[cls][0], cov=postriorParams[cls][1])
        for i in range(testSize):
            res.append(var.pdf(test[i, :]))
        return res

    elif pdfMethod=='knn':
        res = []
        knns = findKnn(train[np.where(trainLabels==cls)[0],:], test, k)
        for i in range(testSize):
            res.append(knns[i][1])
        return res


def bayesClassifier(train, trainLabels, test, classes, landaR=0.0, landaS=0.0, pdfMethod='ML', parzenWindow=1, phiFunction='rec', k=5):
    priors = findPriors(trainLabels, classes)

    testSize = test.shape[0]
    res = [-1 for i in range(testSize)]
    bests = [-1 for i in range(testSize)]
    probSum = [0.0 for i in range(testSize)]
    secondConfidence = [0.0 for i in range(testSize)]
    for cls in classes:
        posteriors = getPosteriors(train, trainLabels, test, classes, pdfMethod, parzenWindow, phiFunction, cls, k)
        print "postriors computed"

        for i in range(testSize):
            val = posteriors[i] * priors[cls]
            if val>bests[i] :
                secondConfidence[i] = bests[i]
                bests[i] = val
                res[i] = cls
            elif val>secondConfidence[i]:
                secondConfidence[i] = val
            probSum[i] += val

    ### for rag bag class
    if landaS != 0:
        for i in range(testSize):
            if bests[i] < (landaS-landaR)*probSum[i] /landaS:
                bests[i] = (landaS-landaR)*probSum[i] /landaS
                res[i] = 'ragBag'

    for i in range(testSize):
        if bests[i] != 0:
            bests[i] = (bests[i] - secondConfidence[i])/bests[i]
    return res, bests



def calculateAccuracy(trueLabels, resLabels):
    return float(len(np.where(trueLabels==resLabels)[0]))/len(resLabels)



res, confidences = bayesClassifier(train_data, train_labels, test_data[:1000, :], class_names, pdfMethod='ML', parzenWindow=1.65, phiFunction='rec', k=3)
print(res)
print(calculateAccuracy(test_labels[:1000], res))
getConfustionConfidenceMatrix(test_labels[:1000], res, class_names, confidences)