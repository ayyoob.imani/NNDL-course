__author__ = 'vandermonde'


def f(n):
    if n <= 1:
        return ["A", "B", "C", "D"]
    else:
        ret = f(n-1)
        res = []
        for item in ret:
            res.append(item + "A")
            res.append(item + "B")
            res.append(item + "C")
            res.append(item + "D")

    return res

print f(2)
exit(0)

from sklearn import svm
from sklearn.model_selection import GridSearchCV
import pandas as pd
from TinyMNIST_loader_withoutPrep import *




class_names = [ i for i in range(10)]
params = {'C':[i+1 for i in range(9)], 'degree':[i+1 for i in range(9)]}

svc = svm.SVC(kernel='poly', gamma=1, decision_function_shape="ovo")
gridSearch = GridSearchCV(estimator=svc, param_grid=params, verbose=1)
gridSearch.fit(train_data, train_labels)
pd.DataFrame(gridSearch.cv_results_).to_csv("oneVsOne.res")


svc = svm.SVC(kernel='poly', gamma=1, decision_function_shape="ovr")
gridSearch = GridSearchCV(estimator=svc, param_grid=params, verbose=1)
gridSearch.fit(train_data, train_labels)
pd.DataFrame(gridSearch.cv_results_).to_csv("oneVsRest.res")


svc = svm.SVC(kernel='linear', gamma=1, decision_function_shape="ovo")
gridSearch = GridSearchCV(estimator=svc, param_grid=params, verbose=1)
gridSearch.fit(train_data, train_labels)
pd.DataFrame(gridSearch.cv_results_).to_csv("LinearoneVsOne.res")


svc = svm.SVC(kernel='linear', gamma=1, decision_function_shape="ovr")
gridSearch = GridSearchCV(estimator=svc, param_grid=params, verbose=1)
gridSearch.fit(train_data, train_labels)
pd.DataFrame(gridSearch.cv_results_).to_csv("LinearoneVsRest.res")
