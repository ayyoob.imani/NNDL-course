__author__ = 'vandermonde'

import numpy as np
from sklearn import neighbors
from sklearn.metrics import accuracy_score
from PR_HW3_P5_knnPDF import getConfustionConfidenceMatrix
from sklearn.naive_bayes import GaussianNB

train_data = np.loadtxt('TinyMNIST/newTrain.txt', dtype=np.float32)
train_labels = np.loadtxt('TinyMNIST/newTrainLabels.txt', dtype=np.int32)
test_data = np.loadtxt('TinyMNIST/newTest.txt', dtype=np.float32)
test_labels = np.loadtxt('TinyMNIST/newTestLabels.txt', dtype=np.int32)
class_names = [i for i in range(10)]


knn = neighbors.KNeighborsClassifier(n_neighbors=1)
model = knn.fit(train_data,train_labels)
predict = model.predict(test_data)

# rnc = neighbors.RadiusNeighborsClassifier(radius=2.87)
# model2 = rnc.fit(train_data,train_labels)
# predict2 = model2.predict(test_data)
#
#
# nb = GaussianNB()
# model3 = nb.fit(train_data,train_labels)
# predict3 = model3.predict(test_data)

accuracy = accuracy_score(test_labels,predict)
print accuracy
getConfustionConfidenceMatrix(test_labels, predict, class_names)