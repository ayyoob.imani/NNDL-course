__author__ = 'vandermonde'

import numpy as np
import math
from plot_conf_mat import plot_confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score

def getConfustionConfidenceMatrix(trueLabels, testLabels, classes, confidencesvals=None):
    cCount = len(classes)
    confusion = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))
    confidences = np.ndarray(shape=(len(classes), len(classes)), dtype=float, order='c', buffer=np.array([0.0 for i in range(cCount**2)]))

    for i in range(len(testLabels)):
        confusion[trueLabels[i], testLabels[i]] += 1
        confidences[trueLabels[i], testLabels[i]] += confidencesvals[i]

    normalizedConfusion = confusion.copy()
    normalizedConfidence = confidences.copy()

    for i in range(cCount):
        np_sum = np.sum(confusion[i, :])
        if np_sum == 0:
            np_sum = 1
        normalizedConfusion[i, :] /= np_sum
        for j in range(cCount):
            if confusion[i,j] == 0:
                pass
            else:
                pass
                normalizedConfidence[i,j] /= confusion[i,j]

    plt.figure()
    plot_confusion_matrix(confusion, classes=classes,
                          title='Confusion matrix, without normalization')
    plt.show()
    plot_confusion_matrix(normalizedConfusion, classes=classes,
                          title='Normalized Confusion', normalize=True)
    plt.show()
    plot_confusion_matrix(normalizedConfidence, classes=classes,
                          title='Normalized Confidence', normalize=True)
    plt.show()


def findKnn(data, x, k=5):
    D = data.shape[1]
    computations = {} #sampleNom:{dist:dataNom}
    Datacount = data.shape[0]
    sampleCount = x.shape[0]

    print("computing : " + str(Datacount) + " * " + str(sampleCount))
    for i in range(Datacount):
        for j in range(sampleCount):
            if j not in computations.keys():
                computations[j] = {}
            computations[j][math.sqrt(np.matmul(data[i, :]-x[j, :], data[i, :]-x[j, :]))] = i
        if i % 100 == 0:
            print (i)

    res = {}
    for i in range(sampleCount):
        s = sorted(computations[i].keys())
        res[i] = ([], (k-1) / (Datacount * s[k-1]**D) )
        for j in range(k):
            res[i][0].append(computations[i][s[j]])

    return res


def knnClassifier(train, trainLabels, test, classes, k=5):
    sampleCount = test.shape[0]
    res = []
    knns = findKnn(train, test, k)

    bests = [-1 for i in range(sampleCount)]
    secs = [-1 for i in range(sampleCount)]
    print ("found knn")
    for i in range(sampleCount):
        counts = {}
        for j in range(k):
            cls = trainLabels[knns[i][0][j]]
            if cls not in counts.keys():
                counts[cls] = 0
            counts[cls] += 1

        bestCls = -1
        for cls in counts.keys():
            if bests[i] < counts[cls]:
                secs[i] = bests[i]
                bests[i] = counts[cls]
                bestCls = cls
            elif secs[i] < counts[cls]:
                secs[i] < counts[cls]

        res.append(bestCls)

    for i in range(sampleCount):
        bests[i] = (bests[i] - secs[i])/bests[i]

    return res, bests

# train_data = np.loadtxt('TinyMNIST/newTrain.txt', dtype=np.float32)
# train_labels = np.loadtxt('TinyMNIST/newTrainLabels.txt', dtype=np.int32)
# test_data = np.loadtxt('TinyMNIST/newTest.txt', dtype=np.float32)
# test_labels = np.loadtxt('TinyMNIST/newTestLabels.txt', dtype=np.int32)
# class_names = [i for i in range(10)]
#
#
# def calculateAccuracy(trueLabels, resLabels):
#     return float(len(np.where(trueLabels==resLabels)[0]))/len(resLabels)
#
# res, confidences = knnClassifier(train_data, train_labels, test_data[:1000,:], class_names, k=1)
# print res
# print(calculateAccuracy(test_labels[:1000], res))
# print(accuracy_score(test_labels[:1000], res))
# getConfustionConfidenceMatrix(test_labels[:1000], res, class_names, confidences)